use std::io::Stdin;

pub fn read_array(input_stream: &Stdin) -> Vec<usize> {
    println!("Enter array size:");
    let array_size: usize = read_usize(input_stream);

    let mut input = String::new();
    println!("Enter array elements:");
    let mut array = vec![0; array_size];
    for i in 0..array_size {
        input.clear();
        input_stream
            .read_line(&mut input)
            .expect("Entered element is not correct");
        array[i] = input.trim().parse::<usize>().unwrap()
    }
    array
}

fn read_usize(input_stream: &Stdin) -> usize {
    let mut input = String::new();
    input_stream
        .read_line(&mut input)
        .expect("Entered array size is not correct!");
    input.trim().parse::<usize>().unwrap()
}

pub fn read_slice_indexes(input_stream: &Stdin) -> (usize, usize) {
    println!("Enter start of the slice:");
    let start: usize = read_usize(input_stream);
    println!("Enter end of the slice:");
    let end: usize = read_usize(input_stream);
    (start, end)
}
