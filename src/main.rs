use std::{io::stdin, usize};
mod input_handler;
mod selection_sort;

use input_handler::{read_array, read_slice_indexes};
use selection_sort::sort;

fn main() {
    let input_stream = stdin();
    let mut array = read_array(&input_stream);
    let (start, end) = read_slice_indexes(&input_stream);
    println!("Your array is:");
    print_slice(&array);
    println!();
    println!("Your slice is:");
    let slice = &mut array[start..end];
    print_slice(slice);
    println!();
    sort(slice);
    println!("Your sorted array is:");
    print_slice(&array);
}

fn print_slice(slice: &[usize]) {
    for i in 0..slice.len() - 1 {
        print!("{}, ", slice[i]);
    }
    print!("{}", slice[slice.len() - 1]);
}
