pub fn sort(array: &mut [usize]) {
    let mut min_index: usize;
    let mut min: usize;
    for i in 0..array.len() {
        min_index = i;
        min = usize::MAX;
        for j in i..array.len() {
            if array[j] < min {
                min_index = j;
                min = array[j];
            }
        }
        swap(&mut array[i], &mut array[min_index]);
    }
}

fn swap(a: *mut usize, b: *mut usize) {
    unsafe {
        let temp = *a;
        *a = *b;
        *b = temp;
    }
}
