# Sort

This is a hobby project written in Rust to explore concepts of Rust. A slice is a new concept introduced to me by Rust language. The sort algorithm used is selection sort.

![](images/sort_example.png)
